import os

from hpc_deployer.webui.app import ui_run
from hpc_deployer.unicore.backend_api import backend_run
from hpc_deployer.utils.config import Config
from hpc_deployer.utils.logger import Logger


import threading

logger = Logger(__name__)

config = Config(os.getcwd())

logger.debug("Start Backend")
threading.Thread(target=backend_run, args=(config,)).start()

logger.debug("Start UI")
threading.Thread(target=ui_run, args=(config,)).start()