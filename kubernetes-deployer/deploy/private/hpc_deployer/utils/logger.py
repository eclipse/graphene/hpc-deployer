import logging

class Logger(logging.Logger):

    def __init__(self, name):
        super().__init__(name)
        formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')

        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.DEBUG)
        console_handler.setFormatter(formatter)
        self.addHandler(console_handler)


        

        