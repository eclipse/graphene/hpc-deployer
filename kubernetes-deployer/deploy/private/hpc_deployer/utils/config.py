import os

class Config:
    def __init__(self, base_path):
        self.base_path = base_path
        self.backend_port = 5001
        self.ui_port = 5000
        self.deployer_backend_url = "http://127.0.0.1:{}".format(self.backend_port)
        self.docker_registery_url = "cicd.ai4eu-dev.eu"
        # Orchestrator configs
        self.orchestrator_name = "orchestrator"
        self.orchestrator_id = 99
        self.orchestrator_host = "localhost"
        self.orchestrator_portgrpc= "9200"
        self.orchestrator_cpu = 4
        self.orchestrator_url = "/generic-parallel-orchestrator/orchestrator_container:1.9-hpc"
        self.orchestrator_start= "python -m ai4eu.orchestratorservice -p {}".format(self.orchestrator_portgrpc)

        self.path_to_blueprint = os.path.join(base_path, "blueprint.json")
        self.path_to_dockerinfo = os.path.join(base_path, "dockerinfo.json")

        #self.create_not_existing_dirs([SHARED_FOLDER,self.status_folder])


    def create_not_existing_dirs(self, list_of_paths):
        for dir in list_of_paths:
            if not os.path.exists(dir):
                # If not, create the folder
                os.makedirs(dir)
                print(f"Folder '{dir}' created successfully.")