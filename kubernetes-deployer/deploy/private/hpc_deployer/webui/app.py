from flask import Flask, render_template, redirect, url_for, session, request,Response
from flask import flash
from flask_bootstrap import Bootstrap
import json
import requests
from urllib.parse import urlparse

from hpc_deployer.utils.config import Config
from hpc_deployer.utils.logger import Logger

logger = Logger(__name__)

config: Config = None
job_status = {
    'job_configured_tag' :False,
    'container_configured_tag' : False
}


app = Flask(__name__)


def save_josn(contents, path):
    with open(path, mode="w") as f:
        json.dump(contents, f)
        f.close()


def read_json(path):
    try:
        # Reading JSON data from the file
        with open(path, mode="r") as file:
            data_read = json.load(file)

        # Check if data is available
        if data_read:
            logger.info("Json read successfully.")
            return data_read
        else:
            logger.info("Json file was empty")

    except FileNotFoundError:
        logger.info(f"File not found: {path}. Writing a new one")

    except json.JSONDecodeError as e:
        logger.error(f"Error decoding JSON: {e}")


def read_node_blueprint():
    list_of_containers =[]
    try:
        input_nodes= read_json(config.path_to_blueprint)
        for dic in input_nodes["nodes"]:
            pull_dic = {
                'image':dic["image"],
                'container_name': dic["container_name"]
            }
            list_of_containers.append(pull_dic)
    except Exception as e:
        logger.error("Couldnt find the blueprint.json")
        logger.error(e, exc_info=True)
        
    return list_of_containers


def read_node_dockerinfo():
    list_of_containers =[]
    try:
        input_nodes= read_json(config.path_to_dockerinfo)
        for dic in input_nodes["docker_info_list"]:
            # Ignore orchestrator.
            if dic["container_name"] == "orchestrator":
                continue
            pull_dic = {
                'container_name': dic["container_name"],
                'ip_address': dic["ip_address"],
                'port': dic["port"],
            }
            list_of_containers.append(pull_dic)
    except Exception as e:
        logger.error("Couldnt find the dockerinfo.json")
        logger.error(e, exc_info=True)
        
    return list_of_containers


def save_dockerinfo(input_list):
    dic = {
        "docker_info_list":input_list
    }
    save_josn(dic,config.path_to_dockerinfo)


def generate_unicore_config():
    for container in containers:
        path = urlparse(container["image_url"]).path
        if path.startswith("7444"):
            path = path[len("7444"):]
        container["image_url"] = config.docker_registery_url + path 

    dic={
        "containers":containers
    }
    dic.update(job_settings)
    return dic
    # path_to_file = "unicore_config.json"
    # save_josn(dic,path_to_file)
    

containers = []
job_settings = {
    "username":"",
    "password":"",
    "site":"JUWELS",
    "project_name":"",
    "gpu_number": 4,
    "partition":"",
    "notification_email":"",
    "memory_per_cpu":4,
    "duration":10,
}


@app.route('/', methods=['GET'])
def index():
    return render_template(
        'index.html', 
        containers=containers, 
        job_settings=job_settings,
        container_configured=job_status["container_configured_tag"], 
        job_configured=job_status["job_configured_tag"]
    )


@app.route('/job', methods=['GET'])
def job():
    return render_template('job.html')


@app.route('/node-config',methods=['POST'])
def node_config():
    global job_status

    form_data = request.form
    id_values = form_data.getlist('id')  # Get all values for the 'id' key
    cpu_values = form_data.getlist('cpu')  # Get all values for the 'cpu' key
    portgrpc_values = form_data.getlist('portgrpc') 
    portwebui_values = form_data.getlist('portwebui') 
    startcommand_values = form_data.getlist('startcommand') 

    saving_dockerinfo = []
    for container in containers:
        id = int(container['id'])
        container['id'] = id_values[id]
        container['cpu'] = cpu_values[id]
        container['portgrpc'] = portgrpc_values[id]
        container['portwebui'] = portwebui_values[id]
        container['startcommand'] = startcommand_values[id]

        dockerinfo ={
            "container_name":container['name'],
            "ip_address":"localhost",
            "port":container['portgrpc']
        }
        saving_dockerinfo.append(dockerinfo)
    
    
    dockerinfo ={
        "container_name":config.orchestrator_name,
        "ip_address":config.orchestrator_host,
        "port":config.orchestrator_portgrpc
    }
    saving_dockerinfo.append(dockerinfo)
    save_dockerinfo(saving_dockerinfo)
    job_status["container_configured_tag"] = True
    return redirect(url_for('index'))


@app.route('/hpc-config',methods=['POST'])
def hpc_config():
    global job_status

    job_settings["username"] = request.form["username"]
    job_settings["password"] = request.form["password"]
    job_settings["site"] = request.form["site"]
    job_settings["duration"] = request.form["duration"]
    job_settings["project_name"] = request.form["project_name"]
    job_settings["gpu_number"] = request.form["gpu_number"]
    job_settings["partition"] = request.form["partition"]
    job_settings["notification_email"] = request.form["notification_email"]
    job_settings["memory_per_cpu"] = request.form["memory_per_cpu"]
    job_status["job_configured_tag"]= True
    return redirect(url_for('index'))


def prepare_input():
    list_of_dockerinfo = read_node_dockerinfo()
    list_of_bluprint = read_node_blueprint()
    http_start_port = 8500
    grpc_start_port = 9500
    for idx, x in enumerate(list_of_dockerinfo):
        docker_url = None
        for ite in list_of_bluprint:
            # if ite["container_name"] == "orchestrator":
            #     continue
            if ite["container_name"] == x["container_name"]:
                docker_url = ite["image"]

        obj_dic = {
            "id": idx,
            "name": x["container_name"],
            "cpu": 4,
            "portgrpc": grpc_start_port,
            "portwebui": http_start_port,
            "startcommand": "python /home/start.py",
            "image_url":docker_url
        }
        containers.append(obj_dic)
        http_start_port +=1
        grpc_start_port +=1


def configure_orchestrator():
    obj_dic = {
        "id": config.orchestrator_id,
        "name": config.orchestrator_name,
        "cpu": config.orchestrator_cpu,
        "portgrpc": config.orchestrator_portgrpc,
        "portwebui": config.orchestrator_portgrpc,
        "startcommand": config.orchestrator_start,
        "image_url":config.orchestrator_url
    }
    containers.append(obj_dic)
    

@app.route('/check-job-started')
def get_status():
    BACKEND_URL = config.deployer_backend_url
    url = f'{BACKEND_URL}/job-started'    
    resp = requests.post(url)
    data = resp.json()
    if data:
        return "true"
    else:
        return "false"


@app.route('/start-orchestrator', methods=['POST'])
def start_orchestrator():
    BACKEND_URL = config.deployer_backend_url
    url = f'{BACKEND_URL}/start-orchestrator'  
    requests.post(url)  
    return "<h1>Job started. Check consol for updates"


@app.route('/start-job', methods=['POST'])
def start_job():
    BACKEND_URL = config.deployer_backend_url
    global job_status
    if not job_status["job_configured_tag"]:
        flash("Job is not yet configured. Please configure it before starting the job.")
        return redirect(url_for('index'))
    if not job_status["container_configured_tag"]:
        flash("Containers are not yet configured. Please configure them before starting the job.")
        return redirect(url_for('index'))
    configure_orchestrator()
    unicore_dic = generate_unicore_config()
    url = f'{BACKEND_URL}/pull-container'
    try:
        requests.post(url, json=unicore_dic)
        return redirect(url_for('job'))
    except Exception as e:
        logger.error(e, exc_info=True)
        logger.error("Error while running request")
        return "<h1>Job failed to start. Please check the logs."
    


    
def ui_run(config_main):
    global config
    config = config_main
    prepare_input()
    app.secret_key = "hpcdeployer"
    bootstrap = Bootstrap(app)
    app.run(host="0.0.0.0", port=config_main.ui_port)
    pass