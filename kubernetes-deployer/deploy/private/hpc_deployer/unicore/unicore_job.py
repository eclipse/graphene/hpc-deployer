from pyunicore.forwarder import Forwarder
import pyunicore.client as uc_client
import pyunicore.credentials as uc_credentials
import requests
from requests.auth import HTTPBasicAuth
import threading
import json, time

from hpc_deployer.utils.logger import Logger



logger = Logger(__name__)


class UnicoreJob:
    def __init__(self, username, password, client_name ):
        registry_url = "https://fzj-unic.fz-juelich.de:9112/FZJ/rest/registries/default_registry"

        # authenticate with username/password
        self.auth_header = HTTPBasicAuth(username, password) 
        credential = uc_credentials.UsernamePassword(username, password)
        r  = uc_client.Registry(credential, registry_url)
        self.client = r.site(client_name)
        self.job = None

    def get_job(self):
        return self.job

    def get_job_name(self):
        return self.job.job_id

    def submit_job(self,job_description):
        logger.info("Job submited")
        self.job = self.client.new_job(job_description=job_description, inputs=[])
        print(json.dumps(self.job.properties, indent = 2))

    def wait_job_started(self):
        logger.info("Job submited. Waiting for the job to start.")
        self.job.poll(uc_client.JobStatus.RUNNING)
        return True
    
    def get_status(self):
        return self.job.status

    def job_started(self):
        return self.get_status() == uc_client.JobStatus.RUNNING
    
    def get_working_dir(self):
        return self.job.working_dir
    
    def wait_job_finished(self):
        logger.info("Waiting for the job to finish.")
        self.job.poll()
        return True
    

    def get_logs(self,url):
        #https://zam2125.zam.kfa-juelich.de:9112/JUWELS/rest/core/storages/IXVsy7O41kq-uspace/files/stderr

        headers = {
            "Accept": "application/octet-stream",  # Specify binary data
        }

        # Send GET request to download data
        response = requests.get(url, headers=headers,auth=self.auth_header)
        if response.status_code == 200:
            return response.content
            
    # There is a bug regarding of pulling apptainers in parallel: https://github.com/apptainer/singularity/issues/3579. We use none parallel in our case
    def generate_pull_config_parallel(self,unicore_config):
        email = unicore_config["notification_email"]
        project= unicore_config["project_name"]
        partition= unicore_config["partition"]
        num_gpu= int(unicore_config["gpu_number"])
        container_list= unicore_config["containers"]
        srun_list=[]
        for job_dic in container_list:
            docker_path = job_dic["image_url"]
            apptainer_name = job_dic["name"]
            
            if num_gpu>0:
                srun_string = "srun --exact --gres=gpu:1 -n1 --cpus-per-task=4 bash -c \"apptainer pull --dir ${{APPTAINER_FOLDER}} {apptainer_name}.sif docker://{docker_path}\" &".format(apptainer_name=apptainer_name, docker_path=docker_path)
            else:
                srun_string = "srun --exact -n1 --cpus-per-task=4 bash -c \"apptainer pull --dir ${{APPTAINER_FOLDER}} {apptainer_name}.sif docker://{docker_path}\" &".format(apptainer_name=apptainer_name, docker_path=docker_path)
            srun_list.append(srun_string)

        scratch_folder_name = "${{SCRATCH_{}}}".format(project)
        data_beginn = [
            "#!/bin/sh",
            "echo Job started",

            "JOBDIR=job1",
            
            "export WRITABLE_DIRECTORY=/tmp/cacheapptainer/${JOBDIR}Cache",
            "export APPTAINER_FOLDER={}/apptainerfolder".format(scratch_folder_name),

            "mkdir -p ${WRITABLE_DIRECTORY}",
            "mkdir -p ${APPTAINER_FOLDER}",

            "export APPTAINER_CACHEDIR=$(mktemp -d -p $WRITABLE_DIRECTORY)",
            "export APPTAINER_TMPDIR=$(mktemp -d -p $WRITABLE_DIRECTORY)",
            "echo ${APPTAINER_CACHEDIR}",
            "echo ${APPTAINER_TMPDIR}"
            ]
        
        data_end = [
            "wait",
            "rm -rf ${APPTAINER_CACHEDIR}",
            "rm -rf ${APPTAINER_TMPDIR}"
        ]

        data_complete = data_beginn + srun_list + data_end

        sbatch_setting = [
            "#!/bin/bash",
            f"#SBATCH --account={project}",
            f"#SBATCH --partition={partition}",
            "#SBATCH --nodes=1",
            "#SBATCH --mem-per-cpu=4G   # memory per CPU core",
            "#SBATCH --time=20",
            "#SBATCH --output=stdout",
            "#SBATCH --error=stderr"
        ]

        if num_gpu>0:
            sbatch_setting.append("#SBATCH --gres=gpu:{}".format(num_gpu))

        job_dic = {
            "Name": "Pull job",
            "User email": email,
            "Job type": "RAW",
            "BSS file": "bss.sh",
            "Project": project,
            "Executable": "/bin/sh",
            "Arguments": ["pull.sh"],
            "Imports": [
                {
                    "To":   "pull.sh",
                    "Data": data_complete
                },
                {
                    "To":   "bss.sh",
                    "Data": sbatch_setting
                }
            ]
        }

        return job_dic
    
    def generate_pull_config(self,unicore_config):
        email = unicore_config["notification_email"]
        project= unicore_config["project_name"]
        partition= unicore_config["partition"]
        num_gpu= int(unicore_config["gpu_number"])
        container_list= unicore_config["containers"]
        srun_list=[]
        for job_dic in container_list:
            docker_path = job_dic["image_url"]
            apptainer_name = job_dic["name"]
            
            if num_gpu>0:
                srun_string = "apptainer pull --dir ${{APPTAINER_FOLDER}} {apptainer_name}.sif docker://{docker_path}".format(apptainer_name=apptainer_name, docker_path=docker_path)
            else:
                srun_string = "apptainer pull --dir ${{APPTAINER_FOLDER}} {apptainer_name}.sif docker://{docker_path}".format(apptainer_name=apptainer_name, docker_path=docker_path)
            srun_list.append(srun_string)

        scratch_folder_name = "${{SCRATCH_{}}}".format(project)
        data_beginn = [
            "#!/bin/sh",
            "echo Job started",

            "JOBDIR=job1",
            
            "export WRITABLE_DIRECTORY=/tmp/cacheapptainer/${JOBDIR}Cache",
            "export APPTAINER_FOLDER={}/apptainerfolder".format(scratch_folder_name),

            "mkdir -p ${WRITABLE_DIRECTORY}",
            "mkdir -p ${APPTAINER_FOLDER}",

            "export APPTAINER_CACHEDIR=$(mktemp -d -p $WRITABLE_DIRECTORY)",
            "export APPTAINER_TMPDIR=$(mktemp -d -p $WRITABLE_DIRECTORY)",
            "echo ${APPTAINER_CACHEDIR}",
            "echo ${APPTAINER_TMPDIR}"
            ]
        
        data_end = [
            "rm -rf ${APPTAINER_CACHEDIR}",
            "rm -rf ${APPTAINER_TMPDIR}"
        ]

        data_complete = data_beginn + srun_list + data_end

        sbatch_setting = [
            "#!/bin/bash",
            f"#SBATCH --account={project}",
            f"#SBATCH --partition={partition}",
            "#SBATCH --nodes=1",
            "#SBATCH --mem-per-cpu=4G   # memory per CPU core",
            "#SBATCH --time=30",
            "#SBATCH --output=stdout",
            "#SBATCH --error=stderr"
        ]

        if num_gpu>0:
            sbatch_setting.append("#SBATCH --gres=gpu:{}".format(num_gpu))

        job_dic = {
            "Name": "Pull job",
            "User email": email,
            "Job type": "RAW",
            "BSS file": "bss.sh",
            "Project": project,
            "Executable": "/bin/sh",
            "Arguments": ["pull.sh"],
            "Imports": [
                {
                    "To":   "pull.sh",
                    "Data": data_complete
                },
                {
                    "To":   "bss.sh",
                    "Data": sbatch_setting
                }
            ]
        }

        return job_dic
    

    def generate_run_config(self,unicore_config):
        email = unicore_config["notification_email"]
        project= unicore_config["project_name"]
        partition= unicore_config["partition"]
        duration= int(unicore_config["duration"])
        num_gpu= int(unicore_config["gpu_number"])
        memory_per_cpu= int(unicore_config["memory_per_cpu"])
        container_list= unicore_config["containers"]

        srun_list=[]
        for job_dic in container_list:
            run_script = job_dic["startcommand"]
            apptainer_name = job_dic["name"]
            number_cpu = job_dic["cpu"]
            if num_gpu>0:
                srun_string = "srun --exact --gres=gpu:1 -n1 --cpus-per-task={number_cpu} bash -c \"apptainer exec --nv ${{APPTAINER_FOLDER}}/{apptainer_name}.sif {run_script}\" &".format(
                    number_cpu=number_cpu,
                    apptainer_name=apptainer_name, 
                    run_script=run_script
                    )
            else:
                srun_string = "srun --exact -n1 --cpus-per-task={number_cpu} bash -c \"apptainer exec --nv ${{APPTAINER_FOLDER}}/{apptainer_name}.sif {run_script}\" &".format(
                    number_cpu=number_cpu, 
                    apptainer_name=apptainer_name, 
                    run_script=run_script
                    )
            srun_list.append(srun_string)

        scratch_folder_name = "${{SCRATCH_{}}}".format(project)
        data_beginn = [
            "#!/bin/sh",
            "APPTAINER_FOLDER={}/apptainerfolder".format(scratch_folder_name),
            ]
        
        data_end = [
            "wait"
        ]

        data_complete = data_beginn + srun_list + data_end

        sbatch_setting = [
            "#!/bin/bash",
            f"#SBATCH --account={project}",
            f"#SBATCH --partition={partition}",
            "#SBATCH --nodes=1",
            f"#SBATCH --mem-per-cpu={memory_per_cpu}G   # memory per CPU core",
            f"#SBATCH --time={duration}",
            "#SBATCH --output=stdout",
            "#SBATCH --error=stderr"
        ]

        if num_gpu>0:
            sbatch_setting.append("#SBATCH --gres=gpu:{}".format(num_gpu))

        job_dic = {
            "Name": "Run job",
            "User email": email,
            "Executable": "/bin/sh",
            "Arguments": ["job1.sh"],
            "Job type": "RAW",
            "BSS file": "bssrun.sh",
            "Project": project,
            "Imports": [
                {
                    "To":   "job1.sh",
                    "Data": data_complete
                },
                {
                    "To":   "bssrun.sh",
                    "Data": sbatch_setting
                }
            ]
        }

        return job_dic


class Portforwarder:

    def __init__(self,job):
        self.job = job
    
    def make_portforward_connection(self, hpc_port, local_port):
        logger.info("Starting portforwarding of port {} of hpc to port {} of local.".format(hpc_port, local_port))
        # ip address with inifiniband interface
        compute_node_i = self.job.bss_details()["NodeList"] + "i"

        forwarder = Forwarder(
            transport=self.job.transport,
            endpoint=self.job.properties["_links"]["forwarding"]["href"],
            service_port=hpc_port,
            service_host=compute_node_i,
            debug=False
            )
        forwarder.run(local_port)
        log_message = "Job still running. Continuing portforwarding {} of hpc to port {} of local.".format(hpc_port, local_port)
        while True:
            try:
                logger.info("portforwaring is still running")
                if(self.job.poll(state=uc_client.JobStatus.SUCCESSFUL, timeout=5)):
                    break
                if(self.job.poll(state=uc_client.JobStatus.FAILED, timeout=5)):
                    logger.info("Job failed. Exiting portforwarding.")
                    break
            except Exception as e:
                pass
            logger.info(log_message)
        

    def start_port_forwarding(self, hpc_port, local_port):
        #threading.Thread(target = forwarder.run, args=local_port)
        #my_thread = threading.Thread(target=forwarder.run(local_port))
        try:
            my_thread = threading.Thread(target=self.make_portforward_connection, args=(int(hpc_port), int(local_port)))
            my_thread.daemon = True
            my_thread.start()
        except Exception as e:
            logger.error("Error while portforwarding {} to {}. Retrying...".format(hpc_port, local_port))
            logger.error(e, exc_info=True)
            self.start_port_forwarding(hpc_port, local_port)
        
            
        # self.kill_forwarding(hpc_port)

    # def kill_forwarding(self,hpc_port):
    #     logger.info("Stopping portforwarding to hpc port {}".format(hpc_port))
    #     p_dic = next(item for item in self.forwarding_list if item["hpc_port"] == hpc_port)
    #     p_dic["obj"].terminate()
