# backend_api.py
from flask import Flask, request, jsonify
import json

from hpc_deployer.utils.config import Config
from orchestrator_client.orchestrator_client import start_orchestrator
from hpc_deployer.unicore.unicore_job import UnicoreJob,Portforwarder
from hpc_deployer.utils.logger import Logger

logger = Logger(__name__)
app = Flask(__name__)

main_job = None
config: Config = None
job_finished = False

@app.route('/')
def index():
    return "<h1>Backend is running! Configure the deployer under: http://127.0.0.1:5000</h1>"


@app.route('/pull-container', methods=['POST'])
def pull_container():
    unicore_config = request.get_json()
    start_job(unicore_config)
    return jsonify({"message": "Job started and completed successfully"}), 200


def start_job(unicore_config):
    try:
        global main_job
        global job_finished
        
        unicore_path = "${{SCRATCH_{}}}/unicore-jobs".format(unicore_config["project_name"])
        logger.info("Request recieved. Now starting a job")
        #### start pulling containers
        pull_job = UnicoreJob(unicore_config["username"],unicore_config["password"], unicore_config["site"])
        pull_job_desc = pull_job.generate_pull_config(unicore_config)
        pull_job.submit_job(pull_job_desc)
        pull_job.wait_job_started()
        logger.info("In the meanwhile. check the logs under the path {}/{}".format(unicore_path, pull_job.get_job_name()))
        pull_job.wait_job_finished()
        
        #### start the container
        main_job = UnicoreJob(unicore_config["username"],unicore_config["password"], unicore_config["site"])
        start_job_desc = main_job.generate_run_config(unicore_config)
        main_job.submit_job(start_job_desc)
        logger.info("In the meanwhile. check the logs under the path {}/{}".format(unicore_path,main_job.get_job_name()))
        logger.info("If the job finishes too soon. There must be a bug. Check the logs.")
        main_job.wait_job_started()

        #### start portforwarding
        job_portforwarder = Portforwarder(main_job.get_job())
        for job_dic in unicore_config["containers"]:
            job_portforwarder.start_port_forwarding(job_dic["portwebui"],job_dic["portwebui"])
                
        main_job.wait_job_finished()
        logger.info("Job finished with the status {}".format(main_job.get_status()))
        job_finished = True
    except Exception as e:
        logger.error(e, exc_info=True)


@app.route('/start-orchestrator', methods=['POST'])
def start_orchestrator_job():
    start_orchestrator("localhost",int(config.orchestrator_portgrpc),config.base_path)


@app.route('/job-started', methods=['POST'])
def job_started():
    resp = json.dumps(False)
    if main_job: 
        resp = json.dumps(main_job.job_started())
    if job_finished:
        resp = json.dumps(True)
    return resp


def backend_run(config_main):
    global config
    config = config_main
    app.secret_key = "hpcdeployer"
    app.run(host="0.0.0.0", port=config_main.backend_port)

# if __name__ == '__main__':
#     app.run(debug=True)  # Run the Flask app
